package com.contact.servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.contact.dao.UserDao;
import com.contact.modul.User;

@WebServlet("/login")
public class LoginServlet extends HttpServlet  {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.jsp");
		requestDispatcher.forward(request, response);		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userName = request.getParameter("username");
		String password = request.getParameter("password");
		
		User user = new User();
		user.getUsername();
		user.getPassword();
				
		UserDao userDao = new UserDao();
		
			try {
				if(userDao.validation(userName, password)){
					HttpSession session = request.getSession();
					session.setAttribute("userName", userName);
					response.sendRedirect("dashboard");		
				}
				else{	
					response.sendRedirect("login.jsp");
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		
	}
	
	}


