package com.contact.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.contact.dao.RequestDao;
import com.contact.modul.Request;

@WebServlet("/dashboard")
public class DashboardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{	
	    List<Request> activedataListRequest = new ArrayList<Request>();
		List<Request> archiveDataListRequest = new ArrayList<Request>();
		 
		RequestDao requestDao = new RequestDao();		
		activedataListRequest = (ArrayList<Request>) requestDao.fetchRequest(true);
		request.setAttribute("activedataListRequest",activedataListRequest);

		archiveDataListRequest = (ArrayList<Request>) requestDao.fetchRequest(false);
		request.setAttribute("archiveDataListRequest",archiveDataListRequest);
				
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("dashboard.jsp");
		requestDispatcher.forward(request, response);
			
		}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
	  int id = Integer.parseInt((request.getParameter("id")));
	
     RequestDao requestDao = new RequestDao();
	 String messageForStatus = new String();
	 int changeStateNo;
	
	 changeStateNo = requestDao.changeState(id);
	 if(changeStateNo>0){
		messageForStatus = "Succesfull";
	 }
	 else{
		messageForStatus = "Not Updated";
	 }	
	 request.setAttribute("messageForStatus", messageForStatus);
	 doGet(request, response);
	
	}
}