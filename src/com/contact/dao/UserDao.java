package com.contact.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {
	String url = "jdbc:postgresql://localhost:5432/postgres";
    String username = "postgres";
    String passwords = "root";
    
    String qry = "select * from logindata where username=? and password=?";  
	
	public boolean validation(String userName, String password) 
	 {
		 try {
			Class.forName("org.postgresql.Driver");
			 Connection connection = DriverManager.getConnection(url,username,passwords);
		     PreparedStatement preparedStatement = connection.prepareStatement(qry);
		     preparedStatement.setString(1, userName);
		     preparedStatement.setString(2, password);
		     
		     ResultSet resultSet = preparedStatement.executeQuery();
		     
		     if(resultSet.next())  {
		    	 return true;
		     }
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}	    
		 return false;
	 }
}

