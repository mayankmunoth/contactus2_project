package com.contact.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.contact.modul.Request;

public class RequestDao {
	String url = "jdbc:postgresql://localhost:5432/postgres";
    String username = "postgres";
    String password = "root";
      
    public Connection getConact()  {
   	 Connection connection = null;
   	 
   	 try {
			Class.forName("org.postgresql.Driver");
		    connection = DriverManager.getConnection(url,username,password);
		    
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		 return connection;
	}
    
	 public void saveRequest(Request request)  {
		 String qry = "insert into contactusdata(name, email, message) values (?,?,?)";
	     PreparedStatement preparedStatement;
		try {
			 preparedStatement = getConact().prepareStatement(qry);
			 preparedStatement.setString(1, request.getFullName());
		     preparedStatement.setString(2, request.getEmail());
		     preparedStatement.setString(3, request.getMessage()); 
		     preparedStatement.execute();     
		} catch (SQLException e) {
			e.printStackTrace();
		}
	     
	    
	 }
	 
	 public List<Request> fetchRequest(boolean status) {
		 List<Request> dataList = new ArrayList<Request>();
		 
		 String qry = "select * from contactusdata where isactive=?";
	     PreparedStatement preparedStatement;
	     
		try {
			preparedStatement = getConact().prepareStatement(qry);
			 preparedStatement.setBoolean(1, status);
		     ResultSet resultSet = preparedStatement.executeQuery();
		     
		     while(resultSet.next())  {
		    	 Request request = new Request();
		    	 request.setId(resultSet.getInt("id"));
		    	 request.setFullName(resultSet.getString("name"));
		    	 request.setEmail(resultSet.getString("email"));
		         request.setMessage(resultSet.getString("message"));
		         request.setStatus(resultSet.getBoolean("isactive"));
		         
		    	 dataList.add(request);    	 
		     }
		} catch (SQLException e) {
			e.printStackTrace();
		}
	    
	     return dataList;
	 }

	public int changeState(int id)  {
		boolean state = true;
		
		String qry = "select isactive from contactusdata where id=?";
	    
	    try {
	    	PreparedStatement preparedStatements = getConact().prepareStatement(qry);
			preparedStatements.setInt(1, id);
			 ResultSet resultSet = preparedStatements.executeQuery();
		       
		     if(resultSet.next())  {
		    	 state=resultSet.getBoolean("isactive"); 
		     }     
		     String qry2 = "update contactusdata set isactive=? where id=?";
		     PreparedStatement preparedStatement = getConact().prepareStatement(qry2);
		     preparedStatement.setBoolean(1,!state);
		     preparedStatement.setInt(2,id);
		     
		     int changestate = preparedStatement.executeUpdate();
		     return changestate;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	     return 0;
	}
    
}