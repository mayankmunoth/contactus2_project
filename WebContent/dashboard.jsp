<%@page import="com.contact.modul.Request"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%  
response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");

if(session.getAttribute("userName")==null)  {
	   response.sendRedirect("login.jsp");
   }
%>
   welcome ${userName}
   

   <h1>Active List</h1>
      <table border ="1" width="500" >
         <tr bgcolor="00FF7F">
          <th><b>Id</b></th>
          <th><b>User Name</b></th>
          <th><b>Email</b></th>
          <th><b>Message</b></th>
          <th><b>State</b></th>
         </tr>
<%
ArrayList<Request> rq = (ArrayList<Request>)request.getAttribute("activedataListRequest");
for(Request ele : rq)
   {%>
 
              <tr>
                <td><%=ele.getId()%></td>
                <td><%=ele.getFullName()%></td>
                <td><%=ele.getEmail()%></td>
                <td><%=ele.getMessage()%></td>
                <td><%=ele.getStatus()%></td>
              </tr>
            <%}%> 
  </table>
  
     <h1>Archive State</h1>
      <table border ="1" width="500" >
         <tr bgcolor="00FF7F">
          <th><b>Id</b></th>
          <th><b>User Name</b></th>
          <th><b>Email</b></th>
          <th><b>Message</b></th>
          <th><b>State</b></th>
         </tr>
<%
   ArrayList<Request> archiveData = (ArrayList<Request>) request.getAttribute("archiveDataListRequest");
   for(Request archiveDataElement: archiveData){%>
 
              <tr>
                <td><%=archiveDataElement.getId()%></td>
                <td><%=archiveDataElement.getFullName()%></td>
                <td><%=archiveDataElement.getEmail()%></td>
                <td><%=archiveDataElement.getMessage()%></td>
                <td><%=archiveDataElement.getStatus()%></td>
              </tr>
            <%}%> 
  </table>
  <br>
  <form action ="dashboard" method ="post">
 Enter Id <input type="text" name="id" required placeholder="Enter id"><br>
<br>
<input type="submit" value="Submit"><br>
</form>
<br>
<form action="logout" method="get">
<input type="submit" value="logout" >
</form>
</body>
</html>